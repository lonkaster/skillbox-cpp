

#include <iostream>

std::string text = "Skillbox homework";

int main()
{
    std::cout << text << '\n';
    std::cout << text.length() << '\n';
    std::cout << text[0] << '\n';
    std::cout << text[text.length()-1] << '\n';
}
